package com.kazakimaru.ch04t3.model

data class Contact(
    val nama: String,
    val phone: String,
    val foto: Int
)