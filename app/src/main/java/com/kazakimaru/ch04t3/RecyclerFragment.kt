package com.kazakimaru.ch04t3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.kazakimaru.ch04t3.adapter.ContactAdapter
import com.kazakimaru.ch04t3.databinding.FragmentRecyclerBinding
import com.kazakimaru.ch04t3.model.Contact


class RecyclerFragment : Fragment() {

    private var _binding: FragmentRecyclerBinding? = null
    private val binding get() = _binding!!

    private lateinit var contactAdapter: ContactAdapter

    private val listContacts = arrayListOf<Contact>(
        Contact("Aji1", "1", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji2", "2", R.drawable.ic_baseline_backup_24),
        Contact("Aji3", "3", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji4", "4", R.drawable.ic_baseline_backup_24),
        Contact("Aji5", "5", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji6", "6", R.drawable.ic_baseline_backup_24),
        Contact("Aji7", "7", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji8", "8", R.drawable.ic_baseline_backup_24),
        Contact("Aji9", "9", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji10", "10", R.drawable.ic_baseline_backup_24),
        Contact("Aji11", "11", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji12", "12", R.drawable.ic_baseline_backup_24),
        Contact("Aji13", "13", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji14", "14", R.drawable.ic_baseline_backup_24),
    )

    private val listContactsUpdate = arrayListOf<Contact>(
        Contact("Aji1", "1", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji2", "2", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji3", "3", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji4", "4", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji5", "5", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji6", "6", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji7", "7", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji8", "8", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji9", "9", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji10", "10", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji11", "11", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji12", "12", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji13", "13", R.drawable.ic_baseline_account_circle_24),
        Contact("Aji14", "14", R.drawable.ic_baseline_account_circle_24),
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRecyclerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        buttonUpdateClicked()
        undoData()
    }

    private fun undoData() {
        binding.btnUndo.setOnClickListener {
            updateWithDiffUtil(listContacts)
        }
    }

    private fun buttonUpdateClicked() {
        binding.btnUpdate.setOnClickListener {
            // cara lama
//            updateData(listContactsUpdate)
            updateWithDiffUtil(listContactsUpdate)
        }
    }

//    private fun updateData(value: ArrayList<Contact>) {
//        contactAdapter.updateData(value)
//        contactAdapter.notifyDataSetChanged()
//    }

    private fun updateWithDiffUtil(value:ArrayList<Contact>) {
        contactAdapter.submitData(value)
    }

    private fun initRecyclerView() {
        contactAdapter = ContactAdapter()
        // cara lama
//        contactAdapter.updateData(listContacts)

        updateWithDiffUtil(listContacts)
        binding.rvData.apply {
//            layoutManager = LinearLayoutManager(requireContext())
            // atau
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = contactAdapter
        }
    }

}