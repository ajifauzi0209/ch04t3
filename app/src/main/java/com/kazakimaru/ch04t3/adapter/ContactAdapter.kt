package com.kazakimaru.ch04t3.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kazakimaru.ch04t3.R
import com.kazakimaru.ch04t3.model.Contact

class ContactAdapter()
    : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    private var listContact = arrayListOf<Contact>()

    private val diffCallback = object : DiffUtil.ItemCallback<Contact>() {
        override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    // cara baru pake diffutil
    fun submitData(items: ArrayList<Contact>) = differ.submitList(items)

    // cara lama update
//    fun updateData(items: ArrayList<Contact>) {
//        this.listContact = items
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return ContactViewHolder(view)
    }

    // Melakukan penetapan data yang akan ditampilkan pada setiap item/baris
    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        // cara lama
//        holder.bind(listContact[position])
        // cara baru pake diffutil
        holder.bind(differ.currentList[position])
    }

    // Memberitahu banyaknya list yang akan ditampilkan
    override fun getItemCount(): Int  {
        // cara lama
//        return listContact.size
        // cara baru pake diffutil
        return differ.currentList.size
    }


    // Class Holder
    inner class ContactViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val tvNama: TextView = view.findViewById(R.id.txt_nama)
        val tvPhone: TextView = view.findViewById(R.id.txt_phone)
        val imgProfile: ImageView = view.findViewById(R.id.img_profile)

        fun bind(item: Contact) {
            imgProfile.setImageResource(item.foto)
            tvNama.text = item.nama
            tvPhone.text = item.phone
        }
    }
}